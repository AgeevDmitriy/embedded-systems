struct packet_data {
  char key;
  char value[2];
};

void send_packet(struct packet_data data) { // value in big-endian
  Serial.write(0x7E); // start byte
  
  Serial.write((byte)0x0); // length (2 bytes)
  Serial.write(0x11);
  
  Serial.write(0x10); // Frame type (Transmit Request - 0x10)
  Serial.write((byte)0x1);

  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write(0xFF);
  Serial.write(0xFF);

  Serial.write(0xFF);
  Serial.write(0xFE);

  Serial.write(0x0);
  Serial.write(0x0);

  // payload data
  Serial.write(data.key);
  Serial.write(data.value[0]);
  Serial.write(data.value[1]);


  long sum = 0x10 + 1 + 0xFF + 0xFF + 0xFF + 0xFE + data.key + data.value[0] + data.value[1];
  Serial.write(0xFF - (sum & 0xFF));

}

byte my_Serial_read() {
  byte ret = Serial.read();
  return ret;
}

struct packet_data read_packet() {
  char values[] = {0, 0};
  struct packet_data data;
  data.key = 0;
  data.value[0] = 0;
  data.value[1] = 0;
  if (Serial.available() >= 4) {
    byte start_byte = 0;
    while (start_byte != 0x7E && Serial.available() > 0) {
      start_byte = my_Serial_read();
    }
    delay(50);
    unsigned short packet_length = ((unsigned short) my_Serial_read()) * 256 + ((unsigned short) my_Serial_read());
    unsigned char packet_type = my_Serial_read();
    if (packet_type != 0x90) {
      for (int i = 0; i < packet_length; i++) {
        byte discard_byte = my_Serial_read();
      }
    } else {
      for (int i = 0; i < 11; i++) {
        byte discard_byte = my_Serial_read();
      }
      data.key = my_Serial_read();
      data.value[0] = my_Serial_read();
      data.value[1] = my_Serial_read();
      byte checksum = my_Serial_read();
    }
  }
  return data;
}
